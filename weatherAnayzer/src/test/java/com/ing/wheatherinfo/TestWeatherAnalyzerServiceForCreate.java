package com.ing.wheatherinfo;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ing.weatherinfo.dao.WeatherInfoDAOImpl;
import com.ing.weatherinfo.domain.WeatherInfo;
import com.ing.weatherinfo.service.WeatherInfoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TestWeatherAnalyzerServiceForCreate {

	WeatherInfo wheatherInfo = null;
	
	@InjectMocks
	WeatherInfoServiceImpl wheatherInfoService;
	
	@Mock
	WeatherInfoDAOImpl wheatherInfoDAO;
	
	@Before
	public void populateWheatherInfo(){
		wheatherInfo = new WeatherInfo();
		wheatherInfo.setHumidity(90);
		wheatherInfo.setTemperature(19.9);
		wheatherInfo.setPlace("Delhi");
		wheatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));
	}
	
	@Test
	public void testWheatherInfoServiceForCreate(){
		
		wheatherInfoService.createWheatherInfo(wheatherInfo);
	}
}
