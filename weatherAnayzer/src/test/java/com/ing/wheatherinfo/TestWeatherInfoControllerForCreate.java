package com.ing.wheatherinfo;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ing.weatherinfo.controller.WeatherAnalyzerController;
import com.ing.weatherinfo.domain.WeatherInfo;
import com.ing.weatherinfo.service.WeatherInfoService;

@RunWith(MockitoJUnitRunner.class)
public class TestWeatherInfoControllerForCreate {

	WeatherInfo wheatherInfo;
	
	@InjectMocks
	WeatherAnalyzerController wheatherAnalyzerController;
	
	@Mock
	WeatherInfoService weatherInfoService;
	
	@Before
	public void prepareWheatherInfo(){
		wheatherInfo = new WeatherInfo();
		wheatherInfo.setHumidity(99);
		wheatherInfo.setPlace("Delhi");
		wheatherInfo.setTemperature(11);
		wheatherInfo.setTimeStamp(new Timestamp(System.currentTimeMillis()));;
	}
	
	@Test
	public void test() {
		
		wheatherAnalyzerController.createWheatherInfo(wheatherInfo);
	}

}
