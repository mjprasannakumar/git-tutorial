package com.ing.weatherinfo.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ing.weatherinfo.dao.WeatherInfoDAO;
import com.ing.weatherinfo.domain.WeatherInfo;
import com.ing.weatherinfo.domain.WeatherInfoComponent;
import com.ing.weatherinfo.exception.InvalidWeatherInfoException;

@Service
public class WeatherInfoServiceImpl implements WeatherInfoService{
	
	@Autowired 
	WeatherInfoDAO wheatherInfoDAO;

	public WeatherInfo createWheatherInfo(WeatherInfo weatherInfo){
		WeatherInfoComponent weatherInfoComponent = getWeatherInfoComponent();
		
		WeatherInfo weatherInfoReturned = null;;
		try {
			WeatherInfo weatherInfoNew = new WeatherInfo();
			weatherInfoNew.setId(weatherInfoComponent.getId());
			weatherInfoNew.setHumidity(weatherInfoComponent.getMain().getHumidity());
			weatherInfoNew.setPlace(weatherInfoComponent.getName());
			weatherInfoNew.setTemperature(weatherInfoComponent.getMain().getTemp());
			weatherInfoReturned = wheatherInfoDAO.createWeatherInfo(weatherInfoNew);
		} catch (InvalidWeatherInfoException e) {
			throw new InvalidWeatherInfoException("Invalid Weather Info");
		}
		return weatherInfoReturned;
	}
	
	public WeatherInfoComponent getWeatherInfoComponent() {
		
		WeatherInfoComponent weatherInfoComponent = null;
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://samples.openweathermap.org/data/2.5/weather?q=London&appid=b1b15e88fa797225412429c1c50c122a1";
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	     
	    ResponseEntity<WeatherInfoComponent> result = restTemplate.exchange(url, HttpMethod.GET, entity, WeatherInfoComponent.class);
	    weatherInfoComponent = result.getBody();
		return weatherInfoComponent;
	}
}
